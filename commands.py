import random
import asyncio
from datetime import datetime
from datetime import timedelta

DATA_PATH = "./data/"

# Random plant fact -----------------------------------------------------------
def random_plantfact():
    # randomly selects a fact from a document
    fpath = DATA_PATH + 'plantFacts.txt'
    lines = open(fpath, encoding='utf-8').read().splitlines()
    line = random.choice(lines)
    return line


async def plantfact(mention):
    # creates the response for the 'plantfact' command  
    response = f'{mention} Did you know...? {random_plantfact()} :herb:'
    return response


# Reminder to water plants ----------------------------------------------------
def secondsToDate(addDays: int) -> int:
    # calculates how many seconds until 'addDays' away at 9am
    reminderdate = datetime.now() + timedelta(days=addDays)
    water_date = datetime(reminderdate.year, reminderdate.month, reminderdate.day, 9, 0, 0)
    time_delta = (water_date - datetime.now())
    return time_delta.total_seconds()


async def water_reminder(mention, message):
    # creates the timer for the reminder for the 'waterplant' command
    contains_digit = any(map(str.isdigit, message.content))

    if contains_digit:
        numbers = [int(s) for s in message.content.split() if s.isdigit()]        
        days = numbers[0]
        seconds = secondsToDate(days)

        # notifies the user that the reminder is set
        response = f'{mention} Reminder set for your plant in {days} day(s) at 9:00 am. :potted_plant:'
        await message.channel.send(content=response)

        # the sleep until calculated time
        await asyncio.sleep(seconds) 
        response = f'{mention} Don\'t forget to water your plant! :droplet:'
    else:
        response = f'{mention} Error setting your plant reminder'

    return response
