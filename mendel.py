# mendel.py
#-*- coding: utf-8 -*-
import os
from dotenv import load_dotenv
import asyncio
import discord

import messageHandler

# Tokens to connect to Discord
load_dotenv()
token = os.getenv('DISCORD_TOKEN')

# Mendel's main class
class Mendel(discord.Client):
    exitCmd = "Goodnight." #Currently not used
    
    async def on_ready(self):
        # when connected to the server
        print(f'Rise and shine!! {client.user} has successfully connected to Discord!')

    
    async def on_message(self, message):
        # when a message is recived      
        if message.author == self.user:
            return #ignore messages from itself

        response = await messageHandler.processMessage(message, self.user)

        if not response == "":
            await message.channel.send(content=response)
            
            # if self.exitCmd in msgText:
            #     sys.exit(0)

# Start Mendel
client = Mendel()
asyncio.run(client.run(token))
