# messageHandeler.py

import commands
import asyncio

# Mendel's mention IDs
MENDEL_MOBILEID = "<@628257306664108033>"
MENDEL_PCID = "<@!628257306664108033>"

async def processMessage(message, botUser):
    # takes the message and has the bot respond
    author = message.author
    text = message.content
    #channel = message.channel
    mentions = message.mentions
    botMentioned = False
    botPrefix = False

    if botUser in mentions:
        botMentioned = True
    
    # checks for Mendel's mentions
    messageParts = text.lower().split()
    if (messageParts[0] == MENDEL_MOBILEID) or (messageParts[0] == MENDEL_PCID):
            botPrefix = True

    # Mendel must be mention first, signaling the message is directed at it
    if botMentioned and botPrefix:
        print(message.content)

        # possibly contains a command, command phrase must come second
        if len(messageParts) >= 1:
            command = messageParts[1]
            
            if command == "plantfact":
                return await commands.plantfact(author.mention)
            elif command =="waterplant":
                return await commands.water_reminder(author.mention, message)
        
        # no known command
        return "Hello, I'm Mendel and I'm just a sprout. I love peas!"
